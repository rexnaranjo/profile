window.addEventListener("scroll", function() {
	var navibar = document.querySelector("#navibar");
	navibar.classList.toggle("sticky", window.scrollY > 0);
});